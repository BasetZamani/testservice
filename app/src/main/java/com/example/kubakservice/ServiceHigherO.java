package com.example.kubakservice;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.CountDownTimer;
//import android.widget.Toast;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class ServiceHigherO extends JobService {
    @Override
    public boolean onStartJob(JobParameters params) {
        SharedPreferences sharedPreferences = getSharedPreferences("servicePrefs", Context.MODE_PRIVATE);
        boolean serviceActivated = sharedPreferences.getBoolean("state", false);
        if (serviceActivated) {
            counter = Integer.parseInt(sharedPreferences.getString("number", "0"));
        } else {
            counter = 0;
        }

        loop(this);
        return true;
    }


    @Override
    public boolean onStopJob(JobParameters params) {
        flag = false;
        return true;
    }


    int counter;
    CountDownTimer countDownTimer;
    boolean flag = true;

    private void loop(final Context context) {
        if (flag) {

            countDownTimer = new CountDownTimer(1000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {
                    counter++;
//                    Toast.makeText(context, "" + counter, Toast.LENGTH_SHORT).show();
                    saveCounter(context);
                    loop(context);
                }
            }.start();
        }

    }

    public void saveCounter(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("servicePrefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefEditor = sharedPreferences.edit();
        prefEditor.putString("number", String.valueOf(counter));
        prefEditor.apply();
    }

}
