package com.example.kubakservice;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

public class StartWhenOnline extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent1) {

        if (Intent.ACTION_BOOT_COMPLETED.equals(intent1.getAction())) {
            SharedPreferences sharedPreferences = context.getSharedPreferences("servicePrefs", Context.MODE_PRIVATE);
            boolean serviceActivated = sharedPreferences.getBoolean("state", false);
            if (serviceActivated) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    ComponentName componentName = new ComponentName(context, ServiceHigherO.class);
                    JobInfo info = new JobInfo.Builder(1, componentName)
                            .setRequiresStorageNotLow(true)
                            .setPersisted(true)
                            .setPeriodic(15000 * 60 * 1000)
                            .build();
                    JobScheduler jobScheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
                    jobScheduler.schedule(info);
                } else {
                    Intent intent = new Intent(context, ServiceUnderO.class);
                    context.startService(intent);
                }

            }


        }


    }
}
